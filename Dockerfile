FROM node:16.20.1-alpine as build-stage
WORKDIR /app
COPY package*.json ./

RUN npx browserslist@latest --update-db

RUN npm install
COPY . .
RUN npm run build

# production stage
FROM nginx:stable-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]