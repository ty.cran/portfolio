# Portfolio

## Repository link
https://gitlab.com/ty.cran/portfolio

## URL
https://www.tylercran.name

## Description
A portfolio that contains an introduction, my resume, and projects that I've worked on. It will pull the data from my 
projects straight from any public GitLab repo that I own.

## Languages
Javascript (VueJS)

## Resources Used
API to pull data from GitLab\
Docker\
CI/CD Pipeline\
Web Scraping: pull information from GitLab\
HTML/CSS\
VueJS 2.0

==================


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
